import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IPosts } from './iposts';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IPosts[]>  {
    return this.http.get<IPosts[]>(`${environment.ENN_POINT1}/posts`)
    .pipe(map(res => {
      return res;
    }));
  }

  public getPostsByUserId(userId: string): Observable<IPosts>{
    let params = new HttpParams();
    params = params.append('userId', userId);
    console.warn('PARAMS', params);
    return this.http.get<IPosts>(`${environment.ENN_POINT}/posts`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }
}
