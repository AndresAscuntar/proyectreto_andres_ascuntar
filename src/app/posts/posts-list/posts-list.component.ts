import { Component, OnInit } from '@angular/core';
import { IPosts } from '../iposts';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit {

  public postsList: IPosts[];

  constructor(private postsServer: PostsService) { }

  ngOnInit() {
    this.postsServer.query()
    .subscribe(res => {
      this.postsList = res;
      console.log('response data', res);
    },
    error => console.error('error',error)
    );
  }

}
