import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IPosts } from '../iposts';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts-create',
  templateUrl: './posts-create.component.html',
  styleUrls: ['./posts-create.component.css']
})
export class PostsCreateComponent implements OnInit {

  postsCreateFormGroup: FormGroup;

  posts: IPosts;

  statusSearchUserId: Boolean = false;

  formsearchPosts = this.formBuilder.group({
    userId: ['']
  });

  constructor(private formBuilder: FormBuilder, private postsService: PostsService) { }

  ngOnInit() {
  }

  searchUserId(){
    console.warn('Data', this.formsearchPosts.value);
    this.postsService.getPostsByUserId(this.formsearchPosts.value.userId)
    .subscribe(res => {
      console.warn('Response posts by userId', res);
      this.posts = res;
      this.statusSearchUserId = true;
    }, error => {
      console.warn('No encontrado', error);
      this.posts = null;
      this.statusSearchUserId = false;
    });

  }

}
