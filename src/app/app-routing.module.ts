import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path:'comments',
    loadChildren: () => import('./comments/comments.module')
    .then(mod => mod.CommentsModule)
  },
  {
    path:'posts',
    loadChildren: () => import('./posts/posts.module')
    .then(mod => mod.PostsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
