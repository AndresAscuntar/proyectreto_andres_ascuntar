import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsComponent } from './comments.component';
import { CommentsListComponent } from './comments-list/comments-list.component';
import { CommentsCreateComponent } from './comments-create/comments-create.component';
import { CommentsRoutingModule } from './comments-routing.module';
import { ReactiveFormsModule } from '@angular/forms'


@NgModule({
  declarations: [CommentsComponent, CommentsListComponent, CommentsCreateComponent],
  imports: [
    CommonModule,
    CommentsRoutingModule,
    ReactiveFormsModule
  ]
})
export class CommentsModule { }
