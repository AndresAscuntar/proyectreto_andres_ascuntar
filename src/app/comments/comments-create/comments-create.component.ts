import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IComments } from '../icomments';
import { CommentsService } from '../comments.service';

@Component({
  selector: 'app-comments-create',
  templateUrl: './comments-create.component.html',
  styleUrls: ['./comments-create.component.css']
})
export class CommentsCreateComponent implements OnInit {

  commentsCreateFormGroup: FormGroup;

  comments: IComments;

  statusSearchPostId: Boolean = false;
  
  formsearchComments = this.formBuilder.group({
    postId: ['']
  });

  constructor(private formBuilder: FormBuilder, private commentsService: CommentsService) { }

  ngOnInit() {
  }

  searchPostId(){
    console.warn('Data', this.formsearchComments.value);
    this.commentsService.getCommentsByPostId(this.formsearchComments.value.postId)
    .subscribe(res => {
      console.warn('Response comments by postId', res);
      this.comments = res;
      this.statusSearchPostId = true;
    }, error => {
      console.warn('No encontrado', error);
      this.comments = null;
      this.statusSearchPostId = false;
    });

  }

}
