import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommentsListComponent } from './comments-list/comments-list.component';
import { CommentsCreateComponent } from './comments-create/comments-create.component';


const routes: Routes = [
  {
    path:'comments-list',
    component:CommentsListComponent
  },
  {
    path:'comments-create',
    component:CommentsCreateComponent
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentsRoutingModule { }
