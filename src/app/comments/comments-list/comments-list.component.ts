import { Component, OnInit } from '@angular/core';
import { IComments } from '../icomments';
import { CommentsService } from '../comments.service';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.css']
})
export class CommentsListComponent implements OnInit {

  public commentsList: IComments[];


  constructor(private commentsServer: CommentsService) { }

  ngOnInit() {
    this.commentsServer.query()
    .subscribe(res => {
      this.commentsList = res;
      console.log('response data', res);
    },
    error => console.error('error',error)
    );
  }

}
